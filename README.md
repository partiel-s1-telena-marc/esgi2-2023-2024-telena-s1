Question 1 : Comment protéger une branche dans GitLab ?

Dans GitLab, vous pouvez protéger une branche en définissant des règles dans les paramètres du projet, sous "Repository" > "Protected branches". Ces règles restreignent les actions telles que la modification directe ou la suppression de la branche, garantissant l'intégrité du code.

Question 2 : Pourquoi protéger des branches dans Git ?

Protéger des branches est crucial pour maintenir la stabilité et l'intégrité du code. Cela empêche les modifications non autorisées sur des branches cruciales comme main et impose des politiques de développement telles que les revues de code avant la fusion.

Question 3 : Qu'est-ce qui peut entraîner des conflits de fusion et comment les résoudre ?

Les conflits de fusion surviennent lorsque des modifications contradictoires sont apportées au même fichier ou à des lignes similaires dans des branches à fusionner. Pour les résoudre, vous devez identifier les zones en conflit, les modifier pour inclure les changements nécessaires, puis finaliser la fusion en effectuant un commit.



Commande 1 : git init : Cette commande est utilisée pour initialiser un nouveau dépôt Git dans un répertoire existant ou vide. Elle crée un nouveau sous-dossier nommé ".git" qui contient tous les fichiers nécessaires pour suivre les modifications du projet.

Commande 2 : git add : Cette commande est utilisée pour ajouter des fichiers modifiés à l'index. Cela signifie que Git commence à suivre les modifications apportées à ces fichiers dans le prochain commit. On l'utilise après avoir effectué des modifications dans des fichiers et avant de les committer.

Commande 3 : git commit : Cette commande est utilisée pour enregistrer les modifications ajoutées à l'index dans l'historique des versions du dépôt Git. Un message de commit est généralement inclus pour expliquer les modifications apportées. C'est une étape importante dans le processus de suivi des changements dans un projet Git.

Commande 4 : git pull : Cette commande est utilisée pour récupérer et fusionner les modifications depuis un dépôt distant dans votre branche locale. Cela est utile lorsque d'autres collaborateurs ont poussé des changements sur le dépôt distant et que vous souhaitez mettre à jour votre copie locale du projet.

Commande 5 : git push : Cette commande est utilisée pour pousser vos commits locaux vers un dépôt distant. Cela permet de partager vos modifications avec d'autres collaborateurs ou de sauvegarder votre travail sur un serveur distant tel que GitHub, GitLab ou Bitbucket.

Commande 6 : git checkout : Cette commande est utilisée pour basculer entre différentes branches dans un dépôt Git. Elle peut également être utilisée pour restaurer des fichiers ou des répertoires à une version précédente, en passant à une branche ou à un commit spécifique.